+++
title = "Gutenberg Themes & Documentation"
weight = 1
[extra]
external_link = "https://gutenbergthemes.codesections.com"
source_link = "https://github.com/codesections/gutenberg-theme-demo"
+++

I maintain the theme demos for the [Gutenberg](https://www.getgutenberg.io/)
static site generator.  This site provides live demos of all current Gutenberg
themes, and shows off the power and flexibility of Gutenberg as a static site
generator.

I have also contributed extensively to the [Gutenberg documentation](https://www.getgutenberg.io/documentation/getting-started/installation/)
and have become one of the highest-volume contributors to Gutenberg even 
though I have not (yet) learned Rust.

