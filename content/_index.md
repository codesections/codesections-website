+++
title = "Home"
date = 2018-06-03T08:50:42-04:00
+++

<h2 class="site-header">I’m Daniel Sockwell, a web developer who started coding in an odd way:  I was a lawyer in New York, when my firm needed a coding lawyer.</h2>

 Lawyers and programmers are both incredibly logical, but somehow they don't always speak the same language.  My goal is to bridge that gap, by being skilled in both domains—and the first step in doing that is leveling up my programming skills.

As part of that process, I've built this site to provide a home for the [projects](/projects) I've built along the way—and to take the opportunity to build a [website that is like Richard Feynman's radios](/projects/codesections-website/).

